![](logo.png)
# LXQt Materia theme

A theme for LXQt desktop based on [Materia](https://github.com/nana-4/materia-theme) GTK theme and [Materia KDE](https://github.com/PapirusDevelopmentTeam/materia-kde) for Plasma 5